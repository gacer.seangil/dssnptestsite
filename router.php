<?php

require_once $_SERVER['DOCUMENT_ROOT'] . "private/scripts/errors.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "private/scripts/extra.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "private/scripts/db.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "private/scripts/redir.php";

/**ABOUT ROUTES
 * Routes are defined minimally in the $routes variable by the route (as the key), and the handler (as val.handler).
 * 
 * key (string) > 
 * A string to be matched by the request URL path. The key must have no leading nor trailing slashes.
 * Keys containing curly bracket segments are assumed to be arbitrary segments.
 * These segments are written to $_EXTRA['URL_SEGMENT_CUSTOM'].
 * 
 * val.handler (string) >
 * A path relative to document root pointing to the PHP script intended to handle the request.
 * 
 * Optional route information is as follows:
 * 
 * val.methods (string[]) >
 * An array of strings that enumerates all allowed methods for the routes. 
 * If it does not exists, the router assumes only 'GET' is allowed.
 * 
 * val.search ([string]: string) >
 * Appends additional search parameters for GET methods under $_GET['extra_search'] as an associative array
 * 
 */
$routes = [
  // No-Handler Pages
  '' => ['handler' => "/public/pages/home.php"],
  'services' => ['handler' => "/public/pages/services.php"],
  'contact-us' => ['handler' => "/public/pages/contact-us.php"],
  'register-form' => ['handler' => "/public/pages/register.php"],
  'login-form' => ['handler' => "/public/pages/login.php"],
  // Handler Scripts
  'register' => ['handler' => "/public/handler/register.php", 'methods' => ["POST"]],
  'login' => ['handler' => "/public/handler/login.php", 'methods' => ["POST"]],
  'logout' => ['handler' => "/public/handler/logout.php", 'methods' => ["POST", "GET"]],
  '/submit/service-request/' => ['handler' => "/public/handler/contact-submit.php", 'methods' => ["POST"]],
  'submit/service-request' => ['handler' => "/public/handler/service-submit.php", 'methods' => ["POST"]],
  // AJAX only Handler Scripts
  'data/services/data' => ['handler' => "/public/ajax/getservices.php", "search" => ["need" => "data"]],
  'data/services/tmpl' => ['handler' => "/public/ajax/getservices.php", "search" => ["need" => "tmpl"]],
];

$request_path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$request_method = $_SERVER['REQUEST_METHOD'];

/////// URL Normalization ///////
/// Remove leading and trailing slashes in URL Path;
while (substr($request_path, -1, 1) == '/') {
  $request_path = substr($request_path, 0, -1);
}
while (substr($request_path, 0, 1) == '/') {
  $request_path = substr($request_path, 1, null);
}
/// Remove consecutive slashes;
preg_replace('/\/{2,}/mi', '/', $request_path);

/////// Route Checking ///////
$request_path_exploded = explode('/', $request_path);
$target_route = null;
foreach ($routes as $route => $routeinfo) {
  $route_exploded = explode('/', $route);
  if (count($route_exploded) != count($request_path_exploded)){
    continue;
  }
  $ismatch = true;
  foreach ($route_exploded as $segindex => $segment) {
    if (@preg_match('/^\{.*\}$/', $segment)) {
      $_EXTRA['URL_SEGMENT_CUSTOM'][substr($segment, 1, -1)] = $request_path_exploded[$segindex];
      continue;
    } else if ($segment != $request_path_exploded[$segindex]) {
      $ismatch = false;
      break;
    }
  }
  if (!$ismatch) {
    $_EXTRA['URL_SEGMENT_CUSTOM'] = [];
    continue;
  } else {
    $target_route = $route;
    continue;
  }
}

/////// Route Viability ///////
if ($target_route === null) {
  require_once GiveErrHandler(404, "That route does not exist.");
  exit;
}
if (!file_exists($_SERVER['DOCUMENT_ROOT'] . $routes[$target_route]['handler'])) {
  require_once GiveErrHandler(404, "That route's handler file is missing.");
  exit;
}

/////// Method Viability ///////
if (!isset($routes[$target_route]['methods'])) $routes[$target_route]['methods'] ??= ['GET'];

if (!in_array($request_method, $routes[$target_route]['methods'])) {
  require_once GiveErrHandler(403, "Forbidden.");
  exit;
}


/////// Route Data Append ///////
if (isset($routes[$target_route]["search"])) {
  foreach ($routes[$target_route]["search"] as $key => $value) {
    $_GET[$key] = $value;
  }
}

require_once ($_SERVER['DOCUMENT_ROOT'] . $routes[$target_route]['handler']);
exit;