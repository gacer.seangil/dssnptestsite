<?php
$_MYSQLI = [
  "hostname" => "127.0.0.1",
  "username" => "root",
  "password" => "",
  "database" => "dssnp - portal",
  "port" => 3306,
  "tables" => [
    "account_details" => [
      "id" => ["int", 11, "NOT NULL AUTO_INCREMENT"],
      "name" => ["varchar", 200, "NOT NULL "],
      "email" => ["varchar", 200, "NOT NULL "],
      "pass" => ["varchar", 200, "NOT NULL "],
      "phonenum" => ["varchar", 200, "NOT NULL "],
      "homeadd" => ["varchar", 200, "NOT NULL "],
      "gender" => ["varchar", 200, "NOT NULL "],
      "birthdate" => ["varchar", 200, "NOT NULL "],
    ],
    "service_trans" => [
      "service_id" => ["int", 11, "NOT NULL AUTO_INCREMENT"],
      "user_id" => ["int", 11, "NOT NULL "],
      "service_type" => ["varchar", 45, "NOT NULL "],
      "request_date" => ["DATETIME", null, "NOT NULL "],
      "appr_date" => ["DATETIME", null, "NOT NULL "],
      "priest" => ["VARCHAR", 45, "NOT NULL "],
      "status" => ["TINYINT", 4, "NOT NULL "],
      "data" => ["LONGTEXT", null, "NOT NULL "],
    ],
    "contact_reqs" => [
      'id' => ["int", 11, "UNIQUE NOT NULL"], 
      'name' => ['varchar', 200, "NOT NULL"],
      'email' => ['varchar', 200, "NOT NULL"],
      'phonenum' => ['varchar', 200, "NOT NULL"],
      'details' => ['varchar', 200, "NOT NULL"],
    ]
  ],
];

function NewMYSQLIConnection(): mysqli
{
  global $_MYSQLI;
  return new mysqli($_MYSQLI['hostname'], $_MYSQLI['username'], $_MYSQLI['password'], $_MYSQLI['database'], $_MYSQLI['port']);
}

function GenerateCreateStatement(string $tblname): string|false
{
  global $_MYSQLI;
  if (!array_key_exists($tblname, $_MYSQLI['tables'])) {
    return false;
  }
  $stmt_head = "CREATE TABLE IF NOT EXISTS $tblname ";
  $stmt_body = "";
  foreach (array_keys($_MYSQLI['tables'][$tblname]) as $fieldname => $fieldprops) {
    $fieldtype = $fieldprops[0];
    $fieldsize = $fieldprops[1];
    $fieldattr = $fieldprops[2];
    $stmt_body .= "`$fieldname` $fieldtype" . ($fieldsize ? "($fieldsize)" : '') . " $fieldattr, ";
  }
  $primaryfield = array_keys($_MYSQLI['tables'][$tblname])[0];
  $stmt_body .= "PRIMARY KEY (`$primaryfield`)";
  return "$stmt_head . ($stmt_body);";
}

function GenerateAllTables() {
  global $_MYSQLI;
  $mysqli = NewMYSQLIConnection();
  foreach ($_MYSQLI["tables"] as $key => $value) {
    $mysqli->execute_query(GenerateCreateStatement($key));
  }
  $mysqli->close();
}

GenerateAllTables();