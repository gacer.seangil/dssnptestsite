<?php 
function Redirect(string $redir_to = "/", string $redir_reason = "Unknown Redirect Reason") {
  @session_start();
  $_SESSION['REDIR_REASON'] = $redir_reason;
  header("Location: $redir_to");
}