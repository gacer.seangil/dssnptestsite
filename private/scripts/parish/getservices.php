<?php

namespace ParishFunctions;
function getServices() {
  return file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/private/data/parish/services.json");
}

function getServiceTemplates() {
  return file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/private/data/parish/servicetemplate.json");
}