<?php

namespace HTMLBUILDER;

abstract class HtmlElement {
  public string $tag;
  public bool $singletag = false;
  public array $props;
  public array $content;
}

class HtmlBuilder extends HtmlElement
{
  static array $singletaggers = ["br"];

  public function __construct(string $tag = "div", ?bool $singletag) {
    $this->tag = $tag;
    if ($singletag !== null) {
      $this->singletag = $singletag;
    } else {
      $this->singletag = in_array($this->tag, $this::$singletaggers);
    }
  }

  public function getProperties() {
    return $this->props;
  }

  public function getProperty(string $prop) {
    return $this->props[$prop];
  }

  public function setProperty(string $prop, ?string $val) {
    if ($val === null) {
      unset($this->props[$prop]);
    } else {
      $this->props[$prop] = $val;
    }
  }

  public function printout() {
    $printout = '<' . $this->tag . ' ';
    
    foreach ($this->props as $prop => $val) {
      $printout .= $prop . '="' . $val . '" ';
    }

    $printout = ">";

    $printout .= $this->singletag ? "" : "</$this->tag>";

    return $printout;
  }
}
