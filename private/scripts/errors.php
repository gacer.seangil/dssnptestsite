<?php

$_ERRORS = [
  400 => "Bad Request",
  401 => "Unauthorized",
  403 => "Forbidden",
  404 => "Not Found",
  405 => "Method Not Allowed",
  408 => "Request Timeout",
  409 => "Conflict",
  500 => "Internal Server Error",
];

function GiveErrHandler(int $error_code = 500, string $error_reason = "Unknown.", false | string $handlerfile = false) : string {
  global $_ERRORS;
  http_response_code($error_code);
  $_ERRORS["ERR_REASON"] = $error_reason;
  return $_SERVER['DOCUMENT_ROOT'] . 'public/pages/error/' . (!$handlerfile ? "generic.php" : $handlerfile);
}