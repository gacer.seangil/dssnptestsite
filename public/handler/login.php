<?php
session_start();
if (!isset($_POST['email']) || empty($_POST['email'])) {
  Redirect("/login-form/", "Email was not provided at log-in");
  session_destroy();
  exit;
}
if (!isset($_POST['password']) || empty($_POST['password'])) {
  session_destroy();
  Redirect("/login-form/", "Password was not provided at log-in");
  exit;
}

$mysqli = NewMYSQLIConnection();
$stmt = $mysqli->prepare("SELECT * FROM `account_details` WHERE `email`=? and `pass`=?");
$stmt->bind_param("ss", $_POST['email'], $_POST['password']);
$stmt->execute();
$res = $stmt->get_result();

if ($res->num_rows < 1) {
  Redirect("/login-form/", "Email or Password may be wrong");
  exit;
}

session_start();
$row = $res->fetch_assoc();
if ($row['email'] == $_POST['email'] && $row['pass'] == $_POST['password']) {
  $_SESSION['USER_TYPE'] = "USER";
}

if ($row['admin'] == 1) {
  $_SESSION['USER_TYPE'] = "ADMIN";
}


Redirect("/", "Login Successful");