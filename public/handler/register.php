<?php
require_once ("private/scripts/db.php");

$full_name = $_POST["name_first"] . $_POST["name_mid"] . $_POST["name_last"];
$email = $_POST["email"];
$phonenum = $_POST["phone"];

$acctdetails = [
  'name' => $_POST['name_first'] . $_POST['name_mid'] . $_POST['name_last'],
  'email' => $_POST['email'],
  'pass' => $_POST['email'],
  'phonenum' => $_POST['phone'],
  'homeadd' => $_POST['homeaddr'],
  'gender' => $_POST['gender'],
  'birthdate' => $_POST['bmonth'] . '/' . $_POST['bday'] . '/' . $_POST['byear'],
];

$pquery = $mysqli->prepare("INSERT INTO `account_details` (`name`, `email`, `pass`, `phonenum`, `homeadd`, `gender`, `birthdate`) VALUES (?, ?, ?, ?, ?, ?, ?)");
$pquery->bind_param(
  "sssssss",
  $acctdetails['name'],
  $acctdetails['email'],
  $acctdetails['pass'],
  $acctdetails['phonenum'],
  $acctdetails['homeadd'],
  $acctdetails['gender'],
  $acctdetails['birthdate']
);

@session_start();

if ($pquery->execute()) {
  $_SESSION['BANNER_BGCOLOR'] = "darkgreen";
  $_SESSION['BANNER_COLOR'] = "white";
  $_SESSION['REASON'] = "Registration Successful!";
  Redirect("/", $_SESSION['REASON']); 
}
else {
  $_SESSION['BANNER_BGCOLOR'] = "red";
  $_SESSION['BANNER_COLOR'] = "black";
  $_SESSION['REASON'] = $mysqli->error;
  Redirect("/", $_SESSION['REASON']); 
}

