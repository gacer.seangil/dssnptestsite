import { MergeObjects } from "/public/rsc/js/lib/merge_obj.js";
import * as ModalControls from "/public/rsc/js/lib/modal_controls.js";

async function GetServices() {
  const resp_data = await fetch("/data/services/data");
  const resp_tmpl = await fetch("/data/services/tmpl");
  const json_data = await resp_data.json();
  const json_tmpl = await resp_tmpl.json();

  const services: { [index: string]: any } = {};

  for (const serv_key in json_data) {
    const service = json_data[serv_key];
    services[serv_key] = MergeObjects(json_tmpl, service);
  }
  return services;
}

function CreateServiceForm(ServiceKey: string) {
  const Service = ServicesAvail[ServiceKey];
  const Modal = ModalControls.GetModal();
  while (!Modal.classList.contains("visible")) {
    ModalControls.ToggleModal();
  }
  if (!Service) return false;
  const form = document.createElement("form");
  form.action = "/submit/service-request/";
  form.method = "POST";
  const form_header = document.createElement("img");
  form_header.src = "/public/rsc/img/full-logo.png";
  const form_title = document.createElement("h3");
  form_title.textContent = Service["Proper"];
  const form_proper = document.createElement("div");
  form.insertAdjacentElement("beforeend", form_header);
  form.insertAdjacentElement("beforeend", form_title);
  form.insertAdjacentElement("beforeend", form_proper);

  let cont_row: null | HTMLDivElement = null;
  for (const key in Service["ClientData"]) {
    const field = Service["ClientData"][key];
    if (!Object.keys(field).length) {
      const divider = document.createElement("h4");
      divider.textContent = key;
      form_proper.insertAdjacentElement("beforeend", divider);
      continue;
    }
    if (field["Break"] === false) {
      cont_row ??= document.createElement("div");
      cont_row.classList.add("row");
    }
    const cont_col = document.createElement("div");
    cont_col.classList.add("col");
    const label = document.createElement("label");
    label.textContent = field["Proper"];
    cont_col.insertAdjacentElement("beforeend", label);
    switch (field["Type"]) {
      case "radio":
        const radio_row = document.createElement("div");
        radio_row.classList.add("row");
        field["RadioOpts"].forEach((radio_option: string) => {
          const input = document.createElement("input");
          const sublabel = document.createElement("label");
          input.name = key;
          input.size = 1;
          input.type = field["Type"];
          input.value = radio_option;
          sublabel.textContent = radio_option;
          radio_row.insertAdjacentElement("beforeend", input);
          input.insertAdjacentElement("afterend", sublabel);
        });
        cont_col.insertAdjacentElement("beforeend", radio_row);
        break;

      default:
        const input = document.createElement("input");
        input.type = field["Type"];
        input.name = key;
        input.size = 1;
        cont_col.insertAdjacentElement("beforeend", label);
        cont_col.insertAdjacentElement("beforeend", input);
        break;
    }
    if (!cont_row) {
      form_proper.insertAdjacentElement("beforeend", cont_col);
      continue;
    }
    cont_row.insertAdjacentElement("beforeend", cont_col);
    if (field["Break"] === true || field["Break"] == undefined) {
      form_proper.insertAdjacentElement("beforeend", cont_row);
      cont_row = null;
      continue;
    }
  }

  const astr_divider_up = document.createElement("p");
  astr_divider_up.textContent = "*".repeat(14);
  form_proper.insertAdjacentElement("beforeend", astr_divider_up);

  if (Service["Reminder"]) {
    const divider_reminder = document.createElement("p");
    divider_reminder.textContent = Service["Reminder"];
    const astr_divider_down = document.createElement("p");
    astr_divider_down.textContent = "*".repeat(14);
    form_proper.insertAdjacentElement("beforeend", divider_reminder);
    form_proper.insertAdjacentElement("beforeend", astr_divider_down);
  }

  for (const key in Service["ParishData"]) {
    const field = Service["ParishData"][key];
    if (!Object.keys(field).length) {
      const divider = document.createElement("h4");
      divider.textContent = key;
      form_proper.insertAdjacentElement("beforeend", divider);
      continue;
    }
    if (field["Break"] === false) {
      cont_row ??= document.createElement("div");
      cont_row.classList.add("row");
    }
    const cont_col = document.createElement("div");
    cont_col.classList.add("col");
    const label = document.createElement("label");
    label.textContent = field["Proper"];
    const input = document.createElement("input");
    input.name = key;
    input.type = field["Type"];
    input.disabled = true;
    input.placeholder = field["Placeholder"] ?? "";
    cont_col.insertAdjacentElement("beforeend", label);
    cont_col.insertAdjacentElement("beforeend", input);
    if (!cont_row) {
      form_proper.insertAdjacentElement("beforeend", cont_col);
      continue;
    }
    cont_row.insertAdjacentElement("beforeend", cont_col);
    if (field["Break"] === true || field["Break"] == undefined) {
      form_proper.insertAdjacentElement("beforeend", cont_row);
      cont_row = null;
      continue;
    }
  }

  cont_row = document.createElement("div");
  cont_row.classList.add("row");
  const input_exit = document.createElement("input");
  const input_submit = document.createElement("input");
  input_exit.type = "reset";
  input_exit.addEventListener("click", () => {
    ModalControls.ModalOff();
  })
  input_submit.type = "submit";
  cont_row.insertAdjacentElement("beforeend", input_exit);
  cont_row.insertAdjacentElement("beforeend", input_submit);

  form_proper.insertAdjacentElement("beforeend", cont_row);

  Modal.insertAdjacentElement("beforeend", form);
}

const ServicesAvail = await GetServices();
const ServicePanel = document.getElementById("services-panel");
if (!ServicePanel) throw "SERVICE PANEL IS MISSING";

console.log(await GetServices());

for (const serv_key in ServicesAvail) {
  const serv = ServicesAvail[serv_key];
  const serv_panel = document.createElement("div");
  serv_panel.classList.add("service-panel");
  const serv_panel_img = document.createElement("img");
  serv_panel_img.src = "/public/rsc/img/services/" + serv["Icon"].toString();
  const serv_panel_h2 = document.createElement("h2");
  serv_panel_h2.textContent = serv["Proper"];

  serv_panel.addEventListener("click", () => { CreateServiceForm(serv_key) });

  serv_panel.insertAdjacentElement("afterbegin", serv_panel_img);
  serv_panel.insertAdjacentElement("beforeend", serv_panel_h2);

  ServicePanel.insertAdjacentElement("beforeend", serv_panel);
}