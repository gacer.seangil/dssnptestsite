export function MergeObjects (template: {[index: string]:any}, data: {[index: string]:any}) : {[index: string]:any} {
  for (const tkey in template) {
    if (!data.hasOwnProperty(tkey)) {
      data[tkey] = template[tkey];
    } else if (typeof data[tkey] === "object" && typeof template[tkey] === "object") {
      MergeObjects(template[tkey], data[tkey]);
    }
  }
  return data;
}