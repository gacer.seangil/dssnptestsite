export function ToggleModal() {
  const modal = document.getElementsByClassName("modal")[0];
  if (!modal) return false;
  if (!modal.classList.toggle("visible")) {
    while (modal.firstChild) modal.removeChild(modal.firstChild);
    return true;
  }
  return modal;
}

export function ModalOff() {
  const modal = document.getElementsByClassName("modal")[0];
  if (!modal) return false;
  modal.classList.remove("visible");
  while (modal.firstChild) modal.removeChild(modal.firstChild);
  return;
}

export function GetModal() {
  const modal = document.getElementsByClassName("modal")[0];
  return modal;
}


document.addEventListener("keydown", (ev) => {
  if (ev.key == "Escape") {
    ModalOff();
  }
})