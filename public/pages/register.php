<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/public/rsc/css/style.css">
  <title>Document</title>

  <style>
    body>main>article.first {
      background-image: linear-gradient(to bottom, rgba(0, 0, 0, 10%) 0%, rgba(0, 0, 0, 70%) 50%, rgba(0, 0, 0, 10%) 100%), url("/public/rsc/img/home-firstarticle.jpg");
      align-items: stretch;
      justify-content: ;

    }

    body>main>article>form {
      flex: 0 0;
      padding: 3ch;
      border: 1px solid hsla(0, 0%, 100%, 25%);
      border-radius: 3ch;
      backdrop-filter: blur(1ch);
      align-items: stretch;
      justify-content: stretch;
    }

    body>main>article>form h1,
    body>main>article>form p {
      margin: 0;
    }

    form div.row {
      flex-direction: row;
      margin: 1ch 0px;
      flex: 1 0;
    }

    form div.col {
      flex-direction: column;
      margin: 1ch 0px;
      flex: 1 0;
    }

    form>div.row>input,
    form>div.row>select {
      margin: 0px 1ch;
    }
  </style>
</head>

<body>
  <?php include_once $_SERVER["DOCUMENT_ROOT"] . "public/pages/subpage/header.php" ?>
  <main>
    <article class="first">
      <form action="/register" method="POST">
        <div class="row">
          <div class="col">
            <h1>REGISTRATION</h1>
            <p>Please register to obtain our services!</p>
          </div>
          <div class="col" style="justify-content: center; align-items: center;">
            <img src="/public/rsc/img/full-logo.png" alt="full-logo" style="max-height: 200px; max-width: 200px">
          </div>
        </div>
        <div class="row">
          <input type="text" required name="full_name" id="reg_form_full_name" placeholder="Full Name"
            style="flex: 1 0;">
          <input type="text" required name="email" id="reg_form_email" placeholder="Email" style="flex: 1 0;">
        </div>
        <div class="row">
          <input type="text" required name="pass" id="reg_form_pass" placeholder="Password" style="flex: 6 0;">
          <input type="text" required name="confirmpass" id="reg_form_confirmpass" placeholder="Confirm Password"
            style="flex: 6 0;">
        </div>
        <div class="row">
          <input type="text" required name="phone" id="reg_form_phone" placeholder="Phone Number" style="flex: 4 0;">
        </div>
        <div class="row">
          <input type="text" required name="homeaddr" id="reg_form_homeaddr" placeholder="Home Address"
            style="flex: 12 0;">
        </div>
        <div class="row">
          <select required name="gender" id="reg_form_gender" placeholder="Gender" style="flex: 3 0;">
            <option selected>Select a Gender...</option>
            <option value="Male">Male</option>
            <option value="Female">Female</option>
          </select>
          <select required name="bmonth" id="reg_form_bmmonth" placeholder="Birth Month" style="flex: 3 0;">
            <option selected>Select a Month...</option>
            <option value="1">January</option>
            <option value="2">February</option>
            <option value="3">March</option>
            <option value="4">April</option>
            <option value="5">May</option>
            <option value="6">June</option>
            <option value="7">July</option>
            <option value="8">August</option>
            <option value="9">September</option>
            <option value="10">October</option>
            <option value="11">November</option>
            <option value="12">December</option>
          </select>
          <input type="text" required name="bday" id="reg_form_bday" placeholder="Birth Day" style="flex: 1 0;">
          <input type="text" required name="byear" id="reg_form_byear" placeholder="Birth Year" style="flex: 3 0;">
        </div>
        <input type="submit" value="Create Account">
      </form>
    </article>
  </main>
  <?php include_once $_SERVER["DOCUMENT_ROOT"] . "public/pages/subpage/footer.php" ?>
</body>

</html>