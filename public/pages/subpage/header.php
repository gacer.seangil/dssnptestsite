<?php @session_start();?>
<header>
  <a href="/" class="h-logo"><img src="/public/rsc/img/full-logo.png" alt="full-logo"></a>
  <nav>
    <?php
    if (isset($_SESSION['USER_TYPE']) && $_SESSION['USER_TYPE'] != "GUEST") {
      echo '<a href="/services/" id="h-nav-serv"><i></i>Services</a>';
    } 
    ?>
    <a href="/contact-us/" id="h-nav-cont"><i></i>Contact Us</a>
    <a href="/register-form/" id="h-nav-regi"><i></i>Register</a>
    <?php
    @session_start();
    if (!isset($_SESSION['USER_TYPE']) || $_SESSION['USER_TYPE'] == "GUEST" || empty($_SESSION['USER_TYPE'])) {
      echo '<a href="/login-form/" id="h-nav-logi"><i></i>Log In</a>';
    } else {
      echo '<a href="/logout/" id="h-nav-logi"><i></i>Log Out</a>';
    }
    ?>
  </nav>
</header>
<div class="banner" <?php @session_start(); echo isset($_SESSION['REDIR_REASON']) ? 'style="background-color:' . 'gold' . ';' . 'color:' . 'black' . '"': '' ?> ><?php @session_start(); echo isset($_SESSION['REDIR_REASON']) ? $_SESSION['REDIR_REASON'] : '' ?></div>
<?php @session_start(); unset($_SESSION['REDIR_REASON']); ?>