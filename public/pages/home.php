<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/public/rsc/css/style.css">
  <title>Document</title>

  <style>
    body>main>article.first {
      background-image: linear-gradient(to bottom, rgba(0, 0, 0, 10%) 0%, rgba(0, 0, 0, 70%) 50%, rgba(0, 0, 0, 10%) 100%), url("/public/rsc/img/home-firstarticle.jpg");
      justify-content: center;
      align-items: center;
      color: whitesmoke;
      text-align: center;
    }

    body>main>article.first>h1 {
      color: red;
    }
    body>main>article.first>div.acc-buttons {
      flex-direction: row;
    }
    body>main>article.first>div.acc-buttons>a {
      text-decoration: none;
    }
  </style>
</head>

<body>
  <?php include_once $_SERVER["DOCUMENT_ROOT"] . "public/pages/subpage/header.php" ?>
  <main>
    <article class="first">
      <h2>Diocesan Shrine of Sto. Niño Parish</h2>
      <img src="/public/rsc/img/logo.png" style="width: 300px;">
      <h1>DSSNP PORTAL</h1>
      <h3 style="font-weight: normal;">
        " A CHRIST-FILLED COMMUNITY STRENGTHENED BY LOVE AND FAITH FOR THE SERVICE OF GOD AND PEOPLE "
      </h3>
      <div class="acc-buttons">
        <a href="/register-form/"><button type="button" class="lvl-a" id="btn-register" href="">Register</button></a>
        <a href="/login-form/"><button type="button" id="btn-login">Login</button></a>
      </div>
    </article>
  </main>
  <?php include_once $_SERVER["DOCUMENT_ROOT"] . "public/pages/subpage/footer.php" ?>
</body>

</html>