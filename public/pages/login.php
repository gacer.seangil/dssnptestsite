<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/public/rsc/css/style.css">
  <title>Document</title>

  <style>
    body>main>article.first {
      background-image: linear-gradient(to bottom, rgba(0, 0, 0, 10%) 0%, rgba(0, 0, 0, 70%) 50%, rgba(0, 0, 0, 10%) 100%), url("/public/rsc/img/home-firstarticle.jpg");
      justify-content: center;
      align-items: center;
    }

    main>article>form {
      flex: 0 0;
      border-radius: 3ch;
      border: 1px solid hsla(0, 0%, 100%, 25%);
      padding: 3ch;
      backdrop-filter: blur(1ch);
      text-align: center;
    }

    main>article>form>img {
      max-width: 400px;
      max-height: 400px;
    }

    main>article>form>h1 {
      padding: 0;
      margin: 0.5ch;
    }

    main>article>form>input {
      align-self: stretch;
    }
  </style>
</head>

<body>
  <?php include_once $_SERVER["DOCUMENT_ROOT"] . "public/pages/subpage/header.php" ?>
  <main>
    <article class="first">
      <form action="/login" method="POST">
        <img src="/public/rsc/img/logo.png" alt="">
        <h1>
          DSSNP PORTAL
        </h1>
        <input type="text" name="email" id="form_login_email" placeholder="Email">
        <input type="password" name="password" id="form_login_password" placeholder="Password">
        <input type="submit" value="Log In">
      </form>
    </article>
  </main>
  <?php include_once $_SERVER["DOCUMENT_ROOT"] . "public/pages/subpage/footer.php" ?>
</body>

</html>