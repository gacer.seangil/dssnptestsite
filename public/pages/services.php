<?php
session_start();

if (!isset($_SESSION["USER_TYPE"])) {
  $_SESSION['USER_TYPE'] = 'GUEST';
}

if ($_SESSION["USER_TYPE"] == 'GUEST') {
  session_destroy();
  require_once GiveErrHandler(403, "Only Members are allowed to partake in Services.");
  exit;
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/public/rsc/css/style.css">
  <title>Document</title>

  <style>
    body>main>article.first {
      background-image: linear-gradient(to bottom, rgba(0, 0, 0, 10%) 0%, rgba(0, 0, 0, 70%) 50%, rgba(0, 0, 0, 10%) 100%), url("/public/rsc/img/home-firstarticle.jpg");
      justify-content: center;
      align-items: center;
      color: whitesmoke;
    }

    body>main>article.first>div {
      flex: 1 0;
      align-self: stretch;
      align-items: center;
      justify-self: stretch;
      justify-content: center;
      flex-flow: column;
      border: 2px solid hsla(0, 0%, 100%, 25%);
      border-radius: 3ch;
      backdrop-filter: blur(3ch);
      background-color: hsla(0, 0%, 100%, 75%);
    }

    body>main>article.first>div>h1 {
      flex: 0 0;
      color: darkred;
      align-self: flex-start;
      justify-self: flex-start;
      border-left: 2px solid darkred;
      margin: 1em;
      padding-left: 1em;
    }

    body>main>article.first>div>div {
      flex: 1 0;
      flex-flow: row wrap;
      justify-content: center;
      align-items: center;
    }
    body>main>article.first>div>div>div.service-panel {
      flex: 0 0;
      min-height: 300px;
      max-height: 300px;
      min-width: 300px;
      max-width: 300px;
      border: 1px solid hsla(0, 0%, 0%, 25%);
      margin: 2ch;
      border-radius: 2ch;
      align-items: center;
      justify-content: center;
      padding: 1.5ch;
      text-align: center;
      cursor: pointer;
    }
    body>main>article.first>div>div>div.service-panel>img {
      max-height: 200px;
      max-width: 200px;
    }
    body>main>article.first>div>div>div.service-panel>h2 {
      white-space: normal;
      margin: 0;
      color: black;
    }
  </style>

  <script src="/public/rsc/js/services.js" type="module"></script>
</head>

<body>
  <?php include_once $_SERVER["DOCUMENT_ROOT"] . "public/pages/subpage/header.php" ?>
  <main>
    <article class="first">
      <div>
        <h1>Services</h1>
        <div id="services-panel"></div>
      </div>
    </article>
  </main>
  <?php include_once $_SERVER["DOCUMENT_ROOT"] . "public/pages/subpage/footer.php" ?>
  <div class="modal">
  </div>
</body>

</html>