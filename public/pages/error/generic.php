<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/public/rsc/css/style.css">
  <link rel="stylesheet" href="/public/rsc/css/error.css">
  <title>Error <?= http_response_code(); ?> </title>
</head>

<body>
  <?php include_once $_SERVER["DOCUMENT_ROOT"] . "public/pages/subpage/header.php" ?>
  <main>
    <article class="first">
      <h1>Error <?php echo http_response_code(); ?></h1>
      <h2><?= $_ERRORS[http_response_code()] ?? "Unknown Code" ?></h2>
      <h3><?= isset($_ERRORS['ERR_REASON']) ? $_ERRORS['ERR_REASON'] : 'Unspecified Internal Server Error' ?></h3>
    </article>
  </main>
  <?php include_once $_SERVER["DOCUMENT_ROOT"] . "public/pages/subpage/footer.php" ?>
</body>

</html>