<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="/public/rsc/css/style.css">
  <title>Document</title>

  <style>
    body>main>article.first {
      background-image: linear-gradient(to bottom, rgba(0, 0, 0, 10%) 0%, rgba(0, 0, 0, 70%) 50%, rgba(0, 0, 0, 10%) 100%), url("/public/rsc/img/home-firstarticle.jpg");
      align-items: stretch;
    }

    body>main>article>form {
      flex: 1 0;
      padding: 3ch;
      border: 1px solid hsla(0, 0%, 100%, 25%);
      border-radius: 3ch;
      backdrop-filter: blur(1ch);
      align-items: stretch;
      justify-content: stretch;
    }

    form>div.row {
      flex-direction: row;
      margin: 1ch 0px;

    }

    form>div.row>input,
    form>div.row>select {
      margin: 0px 1ch;
    }

    article.first>form {
      align-items: center;
    }

    article.first>form div.col {
      flex: 1 0;
      flex-direction: column;
    }

    article.first>form div.row {
      flex: 0 0;
      flex-direction: row;
    }

    article.first>form div input,
    article.first>form div textarea {
      margin: 1ch;
      flex: 1 0;
      justify-self: stretch;
      align-self: stretch;
    }
  </style>
</head>

<body>
  <?php include_once $_SERVER["DOCUMENT_ROOT"] . "public/pages/subpage/header.php" ?>
  <main>
    <article class="first">
      <form action="//submit/service-request//" method="POST">
        <h1 style="text-transform: capitalize;">Get in <span style="color: red">touch</span> with <span
            style="color: red">us</span></h1>
        <span id="contact-phone">Phone: (02) 8929 4527</span>
        <span id="contact-phone">Email: stoninoparishshrine@gmail.com</span>
        <span id="contact-phone">Address: M26G+3FJ, Bukidnon, Bago Bantay, Lungsod Quezon, Kalakhang Maynila</span>
        <div class="col" style="flex: 1 0; align-self: stretch">
          <div class="row">
            <input type="text" name="name" placeholder="Name">
            <input type="text" name="email" placeholder="Email">
            <input type="text" name="phone_num" placeholder="Phone Number">
          </div>
          <div class="row" style="flex: 1 0; justify-self: stretch">
            <textarea name="comments" placeholder="Comments"></textarea>
          </div>
        </div>
        <input type="submit" value="Send Contact Information">
      </form>
    </article>
  </main>
  <?php include_once $_SERVER["DOCUMENT_ROOT"] . "public/pages/subpage/footer.php" ?>
</body>

</html>