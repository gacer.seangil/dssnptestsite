<?php

require_once ($_SERVER['DOCUMENT_ROOT'] . "/private/scripts/parish/getservices.php");

if (!array_key_exists("need", $_GET)) {
  include GiveErrHandler(400, "Missing GET key 'need'.");
  exit;
}


switch ($_GET['need']) {
  case 'data':
    if (!\ParishFunctions\GetServices()) {
      include GiveErrHandler(500);
      exit;
    }

    echo \ParishFunctions\GetServices();
    exit;
  case 'tmpl':
    if (!\ParishFunctions\getServiceTemplates()) {
      include GiveErrHandler(500);
      exit;
    }

    echo \ParishFunctions\getServiceTemplates();
    exit;
  default:
    include GiveErrHandler(400, "GET key 'need' does not match services data requests.");
    exit;
}